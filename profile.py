﻿import motor.motor_asyncio
from bson.objectid import ObjectId
import config 
#client = motor.motor_asyncio.AsyncIOMotorClient(config.DB_IP)
#db = client.spyke
#db.authenticate(config.DB_USER, config.DB_PW)
from cogs.lib import dbutil
codes = dbutil.codes
stats = dbutil.stats
import cogs.lib.uni as uni
import discord
from discord.ext import commands
import random
from datetime import datetime
import time
import re
import importlib as imp
#import requests
import json
import aiohttp
import i18n
i18n.load_path.append('cogs/lib/locale/locale/profile')
from cogs.lib.locale.locale import localconf
i18n.set('locale', 'en')
i18n.set('fallback', 'en')
#i18n.set('locale', 'es')
#i18n.set('file_format', 'yaml')
weaponList = sorted(uni.weaponList)
import subprocess
from cogs.lib import checks
star = "★"
class Profile():
    '''
    Manage your splatoon profile
    '''
    
    def __init__(self, bot):
        self.bot = bot

    def getValue(self,d,key):
        if key not in d:
            return 'Unset'
        if d is None:
            return 'Unset'
        else:
            return d[key]



    async def __error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            await ctx.send(error)
    '''
    async def createLink(self,ctx,url): 
        api_url = 'https://api.shorte.st/v1/data/url'
        data = {
        'urlToShorten' : url,
        'public-api-token' : config.SHORTEST_TOKEN,
        }
        async with aiohttp.ClientSession() as session: 
            r = await session.post(api_url, data=data) 
            link = await r.json() 
        if link:
            return link['shortenedUrl'] 
        else:
            return False
    '''
    
    async def grabId(self,ctx,did):
        try:
            q = await codes.find_one({"discordId": did})
            itm = q['_id']
        except:
            return False
        if itm:
            return itm
        else:
            return False

    async def addDb(self,ctx,key,value):
        itm = await self.grabId(ctx,str(ctx.author.id))
        #if key or value is None:
         #   return False
        if itm:
            await codes.update_one({"_id" : itm } , {"$set": { key : value}})
            return True
        else:
            return False


    async def check(self,ctx,the_id,entry):
        try:
            if await codes.find_one({"discordId" : the_id }):
                async for doc in codes.find({"discordId": the_id },{'_id':0, entry:1}):
                    qResult = doc.values()
                    theEntry = list(qResult)[0]
                    return theEntry
        except IndexError:
            return 'Unset'

    def checkLoad(self,ld):
        if not ld.startswith('https://loadout.ink'):
            return False
        else:
            return True

    def checkW(self,w):
        wl = weaponList
        chk = [] # norm, upper, cap, title
        if w in wl:
            chk.append(1)

        if w.upper() in wl:
            chk.append(1)
            
        if w.capitalize() in wl:
            chk.append(1)
        
        if w.title() in wl:
            chk.append(1)
        return chk

    @commands.command(name='refresh', hidden=True)
    @commands.is_owner()
    async def refresh(self, ctx):
        try:
            imp.reload(uni)
            weaponList = uni.weaponList
        except Exception as e:
            await ctx.send('ERROR '+ e)
        else:
            embed=discord.Embed(title="SUCCESS RELOAD:", description='lib.cogs.uni', color=0x00ff00)
            await ctx.send(embed=embed)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def localedebug(self,ctx,l):
        l_list = ['jp','es','en','de']
        i18n.set('locale', l)
        await ctx.send(f'[@DEBUG] locale set to {l}')
    
    @commands.command()
    @checks.check_flum()
    async def getfc(self,ctx,did: discord.Member = None ):

        if did is None:
            the_id = str(ctx.author.id)
        else:
            the_id = str(did.id)

        if await self.check(ctx,the_id,'FC'):
            return await ctx.send('Their FC is '+ await self.check(ctx,the_id,'FC'))


        
    @commands.command()
    @checks.check_flum()
    async def weapons(self,ctx):
        return await ctx.send('\n'.join(weaponList))

    @commands.command()
    @checks.check_flum()
    async def sranks(self,ctx):
        sr_ranks = [
        'Unemployed',
        'Apprentice',
        'Part-Timer',
        'Go-Getter',
        'Overachiever',
        'Profreshional',
        ]
        return await ctx.send('\n'.join(sr_ranks))

    @commands.group(help=localconf.profile_help,invoke_without_command=True,aliases=[
        'プロファイール',
        'プロフィール',
        'profil',
        'perfil',
        '轮廓',
        '輪廓',
        ])
    @checks.check_flum()
    async def profile(self,ctx, member: discord.Member = None):

        if member is None:
            theId = str(ctx.message.author.id)
            authName = ctx.author.display_name
            authPic =  ctx.author.avatar_url
        else:
            theId = str(member.id)
            authName = member.display_name
            authPic = member.avatar_url 


        async def check(the_id,entry):
            try:
                if await codes.find_one({"discordId" : the_id }):
                    start_time = time.time()
                    #ourCur = await codes.find({"discordId": the_id },{'_id':0, entry:1})
                    async for doc in codes.find({"discordId": the_id },{'_id':0, entry:1}):
                        qResult = doc.values()
                        theEntry = list(qResult)[0]
                        #print("[@DEBUG] DB check took %s seconds" % (round(time.time() - start_time,1)))
                        return theEntry
                        
            except IndexError:
                return 'Unset'

        async def getProf(the_id):
            if await codes.find_one({"discordId" : the_id }):
                doc = await codes.find({"discordId": the_id },{'_id':0})
                return doc

        def getValue(d,key):
            if key not in d:
                return 'Unset'
            if d is None:
                return 'Unset'
            else:
                return d[key]


        if theId == "340354052715970563":
            return await ctx.send('Mate, are you kiddin me?')

        if not await check(theId,'FC'):
            return await ctx.send('You or they are not in the database! Please register with `!!help profile fc`')


        #prof = await getProf(theId)
        prof = await codes.find_one({"discordId" : theId })
        if prof is None:
            fc = 'Unset'
        else:
            fc = getValue(prof,'FC')

        #print(prof['FC'])


        rm = getValue(prof,'RM').upper()
        tc = getValue(prof,'TC').upper()
        sz = getValue(prof,'SZ').upper()
        cb = getValue(prof,'CB').upper()
        #rkt = getValue(prof,'RKT').upper()

        rx = str(getValue(prof,'rm_xPwr'))
        tx = str(getValue(prof,'tc_xPwr'))
        sx = str(getValue(prof,'sz_xPwr'))
        cx = str(getValue(prof,'cb_xPwr'))
        #rxt = str(getValue(prof,'rkt_xPwr'))
        #print(rx,tx,sx,cx)

        if rm == 'X':
            if rx != "Unset":
                rm += f' ({rx})'
        if tc == 'X':
            if tx != "Unset":
                tc += f' ({tx})'

        if sz == 'X':
            if sx != "Unset":
                sz += f' ({sx})'

        if cb == 'X':
            if cx != "Unset":
                cb += f' ({cx})'
        '''
        if rkt == 'X':
            if rxt != "Unset":
                rkt += f' ({rxt})'
        '''


        ranks = f'Rainmaker: {rm}\nSplat Zones: {sz}\nTower Control: {tc}\nClam Blitz: {cb}'
        #ranks = f'Rainmaker: {rm}\nSplat Zones: {sz}\nTower Control: {tc}\nClam Blitz: {cb}\n Rocket: {rkt}'
        #print('still here')

        
        l1 = getValue(prof,'L1')
        l2 = getValue(prof,'L2')
        l3 = getValue(prof,'L3')
        loadouts = f'[Loadout 1]({l1})\n[Loadout 2]({l2})\n[Loadout 3]({l3})'

        async def checkSushi(clan):
            sushipattern = re.compile(r'(?i)^(team )?sushi ?bar\!?$')
            i2dpattern = re.compile(r'(?i)^(i2d|ink ?(2|to) ?(death|discord))')
            if sushipattern.match(clan.lower()) is not None:
                return 'SushiBar!'
            elif i2dpattern.match(clan.lower()) is not None:
                return 'Ink2Death'
            else:
                return clan


        async def checkMako(m):
            makoPattern = re.compile(r'^[Mm][Aa][Kk][Oo][Mm][Aa][Rr][Tt]$')
            if makoPattern.match(m) is not None:
                return 'MakoMart'
            else:
                return m.title()

        w1 = getValue(prof,'W1')
        w2 = getValue(prof,'W2')
        w3 = getValue(prof,'W3')
        #print('i am still working')

        def nzapCheck(w):
            nzapPattern = re.compile(r'^[nN]\-[zZ][Aa][Pp]\s+8[5,9]$')
            if nzapPattern.match(w) is not None:
                return w.upper()
            else:
                return w.title()

        themWeapons = f'{nzapCheck(w1)}\n{nzapCheck(w2)}\n{nzapCheck(w3)}' #%(nzapCheck(w1),nzapCheck(w2),nzapCheck(w3))

        random.shuffle(uni.squid_colors)
        embed=discord.Embed(color=random.choice(uni.squid_colors))
        embed.set_author(name=authName, icon_url=authPic)
        #print('still working')
        #if await check(theId, 'IGN'):
        #if 'IGN' in prof:
        embed.add_field(name='In game name:', value=getValue(prof,'IGN'), inline=True)
        #else:
        #embed.add_field(name='In game name:', value='Unset', inline=True)
        #print('got ign')

        if getValue(prof,'tier') == 'S':
            embed.add_field(name='Level:', value=str(star)+str(getValue(prof,'level')), inline=True)
        else:
            embed.add_field(name='Level:', value=str(getValue(prof,'level')), inline=True)
        #print('got level')
        #else:
         #   embed.add_field(name='Level:', value='Unset', inline=True)

        #print('making profile')
        embed.add_field(name="FC:", value=fc, inline=True)

        embed.add_field(name="Ranks:", value=ranks, inline=True)

        #if 'team' in prof:
        embed.add_field(name="League team power:", value=getValue(prof,'team'), inline=True)
        #else:
         #   embed.add_field(name="League team power:", value='Unset', inline=True)

        #if 'pair' in prof:    
        embed.add_field(name="League pair power:", value=getValue(prof,'pair'), inline=True)

        embed.add_field(name="Salmon run rank:", value=getValue(prof,'salmon').capitalize(), inline=True)

        embed.add_field(name="Favorite weapons:", value=themWeapons, inline=True)


        embed.add_field(name="Favorite map:", value=await checkMako(getValue(prof,'map')), inline=True)

        embed.add_field(name="Loadouts:", value=loadouts, inline=True)
        

        #if prof['clan']: 
        embed.add_field(name="Clan:", value= await checkSushi(getValue(prof,'clan')), inline=True)
        #else:
         #   embed.add_field(name="Clan:", value='Unset', inline=True)

        #if prof['splat']:
        embed.add_field(name="Splatfest team:", value=getValue(prof,'splat'), inline=True)
        #else:
         #   embed.add_field(name="Splatfest team:", value='Unset', inline=True)

        if theId == "250982256976330754":
            embed.set_footer(text='The one true Morti')

        if theId == "124316478978785283":
            embed.set_footer(text='Oh look, its my creator. Theyre pretty great huh?')
        if theId == '232948417087668235':
            embed.set_footer(text='Active maintainer responsible for translation backend. Nice guy, eh?')
        #log it


        channel = self.bot.get_channel(config.logchannel)
        await channel.send('Posted profile for '+ authName+ ' ' + '(' + theId + ')' +'  at: '+ time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())+' at ' +str(ctx.message.guild)+ ' in '+str(ctx.message.channel))
        await ctx.send(embed=embed)
    
    @profile.command(help=localconf.level_help,aliases=['nivel'])
    async def level(self,ctx, lvl: int, tier: str = None):

        #string = i18n.t('es.level_help')

        if lvl < 0:
            return await ctx.send('You cant be negative level..')
        if not isinstance(lvl, int):
            return await ctx.send('Cmon, that isnt even a number!')
        if int(lvl) > 100:
            return await ctx.send('Mate, level 100 is the max..')
        if tier is not None and tier is 'S':
            itm = await self.grabId(ctx,str(ctx.author.id)) 
            await codes.update_one({"_id" : itm } , {"$set": { "level" : lvl ,"tier" : tier}})
            await ctx.send(ctx.author.mention+' level set to '+star+str(lvl))
        elif tier is None:
            itm = await self.grabId(ctx,str(ctx.author.id))
            await codes.update_one({"_id" : itm } , {"$set": { "level" : lvl ,"tier" : None}})
            await ctx.send(ctx.author.mention +' level set to '+ str(lvl))
        elif tier is not 'S':
            return await ctx.send('U WOT MATE?! S is the only tier i accept!')

    @profile.group(help=localconf.weapon_help)
    async def weapon(self, ctx):
        '''
        '''

    @weapon.command()
    async def W1(self,ctx, *, weapon: str):
        if 1 in self.checkW(weapon):
            if await self.addDb(ctx,'W1',weapon):
                await ctx.send(f'{ctx.message.author.mention} your weapon #1 set is to  {weapon}')
        else:
            return await ctx.send(localconf.wep_error)

    @weapon.command()
    async def W2(self,ctx, *, weapon: str):
        if 1 in self.checkW(weapon):
            if await self.addDb(ctx,'W2',weapon):
                await ctx.send(ctx.message.author.mention+' your weapon #2 set is to '+weapon)
        else:
            return await ctx.send(localconf.wep_error)

    @weapon.command()
    async def W3(self,ctx, *, weapon: str):
        if 1 in self.checkW(weapon):
            if await self.addDb(ctx,'W3',weapon):
                await ctx.send(ctx.message.author.mention+' your weapon #3 set is to '+weapon)
        else:
            return await ctx.send(localconf.wep_error)

    @profile.command()
    async def rank(self,ctx,mode: str, rank: str, power: float = None):
        '''
        Set the rank in your profile! This is fairly straightforward:

        tc = tower control
        rm = rainmaker
        sz = splat zones
        cb = clam blitz

        (This is disabled until confirmed it exists)
        rkt = rocket

        Example:

        !!profile rank tc S+0

        X rank works like this
        
        Example:
        
        !!profile rank tc X 300.4
        
        '''

        def validate(m):
            modeList = ['rm','tc','sz','cb']
            if m not in modeList:
                return False
            else:
                return True
        pattern = re.compile(r'^[AaBbCc](\-|\+)?$|^[Ss](\+[0-9])?$|^[Xx]$')
        #pattern2 = re.compile(r'^[Ss]\+5[1-9]')
        if pattern.match(rank) is not None:
            if validate(mode):    
                if await self.addDb(ctx,mode.upper(),rank):
                    await ctx.send(ctx.message.author.mention+' your '+mode+ " rank has been set")
                    xpat = re.compile(r'[Xx]')
                    if xpat.match(rank) is not None:
                        if power is not None:
                            #await ctx.send('DEBUG: '+mode.lower()+'_xPwr '+ rank)
                            await self.addDb(ctx,mode.lower()+'_xPwr',power)

            else:
                return await ctx.send('That game mode does not exist')
        else:
            return await ctx.send('That rank does not exist')

    @profile.command()
    @checks.check_bot()
    @checks.check_flum()
    async def tz(self,ctx,tz : str):
        '''
        This allows you to set a timezone where you live for when you use the salmon run command.
        
        IT IS CASE SENSITIVE!
        
        Please be sure and check https://hastebin.com/ugimufopab.vbs for your proper timezone
        '''
        errormsg = '''That is not a valid timezone. This is CASE SENSITIVE!
please check  https://hastebin.com/ugimufopab.vbs to get your proper timezone!
'''
        if tz not in uni.timezones:
            return await ctx.send(errormsg)
        else:
            await self.addDb(ctx,'tz',tz)
            return await ctx.send(f' {ctx.author.mention} your timezone has been set')


    
    @profile.group()
    async def power(self,ctx):
        '''
        Show off your highest league power!

        Now with both league types! Teams and pair. Requires a decimal
        
        Example:

        !!profile power team 1750.0
        !!profile power pair 1750.0
        '''

    @power.command()
    async def team(self,ctx,pwr: str):
        maxLen = 6
        if float(pwr) < 1:
            return await ctx.send('You cannot be below one!')
        try:
            if len(pwr.split('.')[1]) > 1:
                return await ctx.send('You cannot have more than one decimal place!')
        except IndexError:
            maxLen = 4 #no DP passed
        if float(pwr) < 0:
            return await ctx.send('Mate..you cant be negative power..')

        if len(pwr) > maxLen:
            return await ctx.send('Mate, that is far too high! Theres no way youre that good')
        try:
            if float(pwr):
                if await self.addDb(ctx,'team',pwr):
                    return await ctx.send(ctx.message.author.mention+' your highest team power level has been set to '+pwr ) 

        except ValueError:
            return await ctx.send('Mate, that isnt a number')

    @power.command()
    async def pair(self,ctx,pwr :str):
        maxLen = 6
        if float(pwr) < 1:
            return await ctx.send('You cannot be below one!')
        try:
            if len(pwr.split('.')[1]) > 1:
                return await ctx.send('You cannot have more than one decimal place!')
        except IndexError:
            maxLen = 4 #no DP passed
        if float(pwr) < 0:
            return await ctx.send('Mate..you cant be negative power..')

        if len(pwr) > maxLen:
            return await ctx.send('Mate, that is far too high! Theres no way youre that good')
        try:
            if float(pwr):
                if await self.addDb(ctx,'pair',pwr):
                    return await ctx.send(ctx.message.author.mention+' your highest pair power level has been set to '+pwr ) 

        except ValueError:
            return await ctx.send('Mate, that isnt a number')

    @power.command()
    async def x(self,ctx,pwr: str):
        '''
        Placeholder
        '''
        pass
    
    @profile.command()
    async def fc(self,ctx, sw: str):
        '''
        Register your FC with spyke.

        Example:

        !!profile fc SW-5208-7719-6394
        '''
        channel = self.bot.get_channel(374363982133460993)
        this_id = str(ctx.message.author.id)
        sw = sw.strip().strip('SW').replace('-','')
        formattedSW = 'SW-'+sw[:4]+'-'+sw[4:8]+'-'+sw[8:]
        if re.search(r'^520877196394$',sw) is not None:
            return await ctx.send('Mate please, are ya stupid? Thats the example FC!')

        if re.search(r'^000000000000$',sw) is not None:
            return await ctx.send('Your FC cannot be zero.')

        if re.search(r'^\d{12}$',sw) is not None and len(sw) == 12:
            if await codes.find({'discordId':this_id}).count() > 0:
                itm = await self.grabId(ctx,str(ctx.author.id)) 
                await codes.update_one({"_id" : itm } , {"$set": { "FC" : formattedSW }})
                await ctx.send(ctx.message.author.mention+' your friend code has been updated, love.')
                    
            else:
                await codes.insert({'discordId':this_id, 'FC': formattedSW})
                await ctx.send(ctx.message.author.mention+' you have been added, cheers.')
        else:
            return await ctx.send('Sorry mate, i only accept friend codes..')

    @profile.command()
    async def ign(self,ctx, *,name: str):
        '''
        Set your in game name.

        Example:

        !!profile ign Marie is best waifu
        '''
        if len(name) > 10:
            return ctx.send('Mate, cmon thats too long..')
        if await self.addDb(ctx,'IGN',name):
             await ctx.send(ctx.message.author.mention+' your IGN has been set to '+name )
        else:
            await ctx.send('You arent in the database. Please use `!!profile` to get started ')

    @profile.command()
    async def clan(self,ctx, *,name: str):
        '''
        Set your clan if you have one.

        Example:

        !!profile clan Not Nigiri
        '''
        if len(name) > 50:
            return ctx.send('Mate, cmon thats too long..')
        if await self.addDb(ctx,'clan',name):
             await ctx.send(ctx.message.author.mention+' your clan has been set to '+name )
        else:
            await ctx.send('You arent in the database. Please use `!!profile` to get started ')

    @profile.command()
    async def favmap(self,ctx, *,name: str):
        '''
        Set your favorite map

        Example:

        !!profile favmap Kelp Dome
        '''
        if name.upper() not in [x.upper() for x in uni.privMapList]:
            return await ctx.send('That isnt a map')
        if await self.addDb(ctx,'map',name):
             await ctx.send(ctx.message.author.mention+' your favorite map has been set to '+name )
        else:
            await ctx.send('You arent in the database. Please use `!!profile` to get started ')

    @profile.command()
    async def splat(self,ctx,name: str):
        '''
        Set your current or last splatfest team to help find people to play with

        Example:

        !!profile splat Egg
        '''
        
        async def validate_splatfest(splt):
            headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'}
            async with aiohttp.ClientSession() as session:
                async with session.get("https://splatoon2.ink/data/festivals.json",headers=headers) as r:
                    data = await r.json()
                    alpha_list = []
                    bravo_list = []
                    for i in range(0,len(data['na']["festivals"])):
                        alpha_list.append(data['na']["festivals"][i]['names']['alpha_short'])
                    for i in range(0,len(data['eu']["festivals"])):
                        alpha_list.append(data['eu']["festivals"][i]['names']['alpha_short'])
                    for i in range(0,len(data['jp']["festivals"])):
                        alpha_list.append(data['jp']["festivals"][i]['names']['alpha_short'])
                    
                    for i in range(0,len(data['na']["festivals"])):
                        bravo_list.append(data['na']["festivals"][i]['names']['bravo_short'])
                    for i in range(0,len(data['eu']["festivals"])):
                        bravo_list.append(data['eu']["festivals"][i]['names']['bravo_short'])
                    for i in range(0,len(data['jp']["festivals"])):
                        bravo_list.append(data['jp']["festivals"][i]['names']['bravo_short'])
                    
                    alpha_list = set(alpha_list)
                    bravo_list = set(bravo_list)
                    if splt.title() not in (alpha_list | bravo_list):
                        #await ctx.send(splt in alpha_list)
                        #await ctx.send(splt in bravo_list)
                        #await ctx.send('[@]DEBUG:')
                        #await ctx.send('INPUT:\t'+splt)
                        #await ctx.send(alpha_list)
                        #await ctx.send(bravo_list)
                        return False
                    else:
                        return True
        if await validate_splatfest(name):            
            if len(name) > 30:
                return ctx.send('Mate, cmon thats too long..')
            if await self.addDb(ctx,'splat',name.title()):
                await ctx.send(ctx.message.author.mention+' your splatfest team has been set to '+name )
            else:
                await ctx.send('You arent in the database. Please use `!!profile` to get started ')
        else:
            await ctx.send('That is not a valid splatfest team')

    @profile.command()
    async def sr(self,ctx, *,name: str):
        '''
        Set your salmon rank. Also accepts 300/99 and up to 9999/99

        Example:

        !!profile sr Profreshional
        '''
        pattern = re.compile(r'^[Pp]art-[Tt]imer$|^[Gg]o\-[Gg]etter$|^[Aa]pprentice$|^[Oo]verachiever$|^[Pp]rofreshional$|^\d{3}/99$|^[Uu]nemployed$')
        if pattern.match(name) is not None:
            if await self.addDb(ctx,'salmon',name.capitalize()):
                await ctx.send(ctx.message.author.mention+'your salmon rank has been set to '+name )
        else:
            return await ctx.send(ctx.message.author.mention+' that is not a valid salmon title')
    
    @commands.command()
    @checks.check_flum()
    async def maplist(self,ctx):
        return await ctx.send('\n'.join(uni.privMapList))

    @profile.group()
    async def loadout(self,ctx):
        '''
        Show off your loadout.ink loadouts! You can have up to 3

        Example:

        !!profile loadout L1 https://loadout.ink/url-here
        '''
    @loadout.command()
    async def L1(self,ctx, l: str):
        if l.startswith('https://loadout.ink'):
            '''
            if await self.check(ctx.author.id,'patreon') == 'yes':
                if await self.addDb(ctx,'L1',l):
                    return await ctx.send(ctx.author.mention + ' your loadout #1 has been set without adverts! Thanks for donating, mate')
            '''

            if await self.addDb(ctx,'L1',l):
                return await ctx.send(ctx.author.mention + ' your loadout #1 has been set')
        else:
            return await ctx.send('Mate, you cant fool ol Spyke. Thats not a loadout.ink url')

    @loadout.command()
    async def L2(self,ctx, l: str):
        if self.checkLoad(l):
            if await self.addDb(ctx,'L2',l):
                return await ctx.send(ctx.author.mention + ' your loadout #2 has been set')
        else:
            return await ctx.send('Mate, you cant fool ol Spyke. Thats not a loadout.ink url')

    @loadout.command()
    async def L3(self,ctx, l: str):
        if self.checkLoad(l):
            if await self.addDb(ctx,'L3',l):
                return await ctx.send(ctx.author.mention + ' your loadout #3 has been set')
        else:
            return await ctx.send('Mate, you cant fool ol Spyke. Thats not a loadout.ink url')

    @profile.command()
    async def remove(self,ctx):
        '''
        Remove yourself from the profile database
        '''

        itm = codes.find_one({"discordId": str(ctx.author.id) }).get("_id")
        if itm:
            codes.remove({"_id" : itm })
            await ctx.send(ctx.message.author.mention+' you have been removed..sorry to see you go' )
        else:
            await ctx.send('You were never in the database.')

    @commands.group(invoke_without_command=True)
    @checks.check_bot()
    @checks.check_flum()
    async def sp(self,ctx, member: discord.Member = None):
        '''
        This is the singleplayer part of your profile! Use !!help sp to get started. The subcommands are abbreviated:

        title = title
        compleition percent = cp
        scrolls found = sf
        weapon compleition = wc
        hero weapons obtained = hwo
        best level time = blt

        So check out like, "!!help sp cp" or "!!help sp hwo" to get started adding to your singleplayer profile

        '''

        if member is None:
            theId = str(ctx.message.author.id)
            authName = ctx.author.display_name
            authPic =  ctx.author.avatar_url
        else:
            theId = str(member.id)
            authName = member.display_name
            authPic = member.avatar_url

        if theId == "340354052715970563":
            return await ctx.send('Are you kidding me?')

        prof = await codes.find_one({"discordId" : theId })


        embed=discord.Embed()
        random.shuffle(uni.squid_colors)
        embed=discord.Embed(color=random.choice(uni.squid_colors),title="Singleplayer stats")
        embed.set_author(name=authName, icon_url=authPic)

        embed.add_field(name="Title:", value=str(self.getValue(prof,'title')), inline=True)
    
        embed.add_field(name="Completion percent:", value=str(self.getValue(prof,'cp')+'\%'), inline=True)

        embed.add_field(name="Scrolls found:", value=str(self.getValue(prof,'sf')), inline=True)

        embed.add_field(name="Weapon completion:", value=str(self.getValue(prof,'wc')+'\%'), inline=True)

        embed.add_field(name="Hero weapons obtained:", value=str(self.getValue(prof,'hwo')), inline=True)

        embed.add_field(name="Best level time:", value=str(self.getValue(prof,'blt')) , inline=True)

        embed.add_field(name="Memcakes:", value=str(self.getValue(prof,'mc')) , inline=True)

        embed.add_field(name="Thangs:", value=str(self.getValue(prof,'thangs')) , inline=True)

        embed.add_field(name="Toothpick:", value=str(self.getValue(prof,'pick')) , inline=True)

        await ctx.send(embed=embed)

    @sp.command()
    @checks.check_bot()
    @checks.check_flum()
    async def title(self,ctx,*,name : str):
        '''
        Set your singleplayer title! Check from the nintendo app

        Example:

        !!sp title Dingleberries
        '''
        if await self.addDb(ctx,'title',name):
            return await ctx.send(f'{ctx.author.mention} your singleplayer title has been set')
        
    @sp.command()
    @checks.check_bot()
    @checks.check_flum()
    async def cp(self,ctx,*,num : int):
        '''
        This is your completion percent, you can go up to 1000%

        For now, you cannot use a percent sign so just please enter a number.
        It will show up as a percent in your !!sp profile

        Example:

        !!sp cp 150
        '''
        if num < 0:
            return await ctx.send('You cannot have negative numbers') 
        if num > 1000:
            return await ctx.send('We only go up to 1000 percent..')
        else:  
            if await self.addDb(ctx,'cp',str(num)):
                return await ctx.send(ctx.author.mention + " your completion percent has been set")
            
        
    @sp.command()
    @checks.check_bot()
    @checks.check_flum()
    async def sf(self,ctx,*,num : int):
        '''
        This is to show how many scrolls youve found!

        Example:

        !!sp sf 4
        '''
        if num < 0:
            return await ctx.send('You cannot have negative numbers') 
        if num > 28:
            return await ctx.send('There are only 28 scrolls in the game')
        else:  
            if await self.addDb(ctx,'sf',str(num)):
                return await ctx.send(ctx.author.mention + " your scroll count has been set")

        
    @sp.command()
    @checks.check_bot()
    @checks.check_flum()
    async def wc(self,ctx,*,num : int):
        '''
        Use this to set your weapon completion, how many guns youve bought from ammo knights.

        This is percent based but does not use a percent sign yet. It will show up as a percent in your sp profile

        Example:

        !!sf wc 50
        '''
        if num < 0:
            return await ctx.send('You cannot have negative numbers') 
        if num > 100:
            return await ctx.send('How can you have more than 100 percent of the weapons?')
        else:  
            if await self.addDb(ctx,'wc',str(num)):
                return await ctx.send(ctx.author.mention + " your weapon percent has been set")

        
    @sp.command()
    @checks.check_bot()
    @checks.check_flum()
    async def hwo(self,ctx,*,num : int):
        '''
        This is used to show off how many hero weapons youve obtained

        There are only 9 in the game(? please correct me if im wrong)

        Example:

        !!sp hwo 3
        '''
        if num < 0:
            return await ctx.send('You cannot have negative numbers') 
        if num > 9:
            return await ctx.send('There are only 9 hero weapons in the game!')
        else:  
            if await self.addDb(ctx,'hwo',str(num)):
                return await ctx.send(ctx.author.mention + " number of hero weapons you own has been set")



    @sp.command()
    @checks.check_bot()
    @checks.check_flum()
    async def blt(self,ctx,*,time : str):
        '''
        Set your bacon lettuce tomato!

        Im kidding, this is for your best level time. Use 00:00 format

        Example:

        !!sp blt 01:24
        '''
        pattern= re.compile(r'\d\d\:\d\d')

        if pattern.match(time) is not None:
            if await self.addDb(ctx,'blt',str(time)):
                return await ctx.send(ctx.author.mention + "your best level time has been set")
        else:
            return await ctx.send('That is not a valid time format!')

    @sp.command()
    @checks.check_bot()
    @checks.check_flum()
    async def mc(self,ctx,*,num : int):
        '''
        Use this to show off how many memcakes youve obtained!

        Example:

        !!sp mc 50
        '''
        if num < 0:
            return await ctx.send('You cannot have negative numbers') 
        if num > 80:
            return await ctx.send('There are only 80 levels in the expansion..')
        else:  
            if await self.addDb(ctx,'mc',str(num)):
                return await ctx.send(ctx.author.mention + " your memcake count has been set")

    @sp.command()
    @checks.check_bot()
    @checks.check_flum()
    async def thangs(self,ctx,*,num : int):
        '''
        Set how many thangs youve obtained

        Example:

        !!sp mc 4
        '''
        if num < 0:
            return await ctx.send('You cannot have negative numbers') 
        if num > 4:
            return await ctx.send('There are only 4 thangs')
        else:  
            if await self.addDb(ctx,'thangs',str(num)):
                return await ctx.send(ctx.author.mention + " your thangs has been set")

    @sp.command()
    @checks.check_bot()
    @checks.check_flum()
    async def pick(self,ctx,answer: str):
        '''
        Yes/No if you have the golden toothpick

        Example:

        !!sp pick no

        '''
        pattern = re.compile(r'^[Nn][Oo]$|^[Yy][Ee][Ss]$')
        if pattern.match(answer) is not None:
            if await self.addDb(ctx,'pick',str(answer.title())):
                return await ctx.send(ctx.author.mention + " toothpick set")
        else:
            await ctx.send('This is a yes or no question, mate')
        

        
        

        
        

def setup(bot):
    bot.add_cog(Profile(bot))
